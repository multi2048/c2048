#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include "lib2048.h"

/*
Implementation of 2048 in C. Assume bugs aplenty. Bring bugspray.
I don't know C. At all.
*/

void DisableLineBuffering(void) {
    struct termios tty;
    tcgetattr(STDIN_FILENO, &tty);
    tty.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &tty);
}

void ResetTerminal(void) {
    struct termios tty;
    tcgetattr(STDIN_FILENO, &tty);
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &tty);
}

void PrintAt(int row, int column, char* str) {
    printf("\033[%d;%dH%s",row,column,str);
}

void PrintNumAt(int row, int column, int num) {
    char t[7];
    snprintf(t, 7, "%d", num);
    PrintAt(row, column, t);
}

void PrintBoard(void) {
    int r;
    char lines[2][30] = {
        "+------+------+------+------+",
        "|      |      |      |      |"
    };
    PrintAt( 2, 3, "Rounds:" );
    PrintAt( 3, 3, "Score:" );
    for ( r = 0; r < 9; r++ ) {
        PrintAt( 5+r, 2, lines[r%2] );
    }
    PrintAt( 15, 3, "Move: WASD, HJKL, Arrows" );
    PrintAt( 16, 3, "Quit: Q, X, ." );
}

void PrintState(struct GameState *s) {
    int r,c;
    PrintNumAt( 2, 11, s->rounds );
    PrintNumAt( 3, 11, s->score );
    for ( r = 0; r < 4; r++ ) {
        for ( c = 0 ; c < 4 ; c++ ) {
            PrintAt( 6+2*r, 3+7*c, "      " );
            if ( s->board[r][c] > 0 ) {
                PrintNumAt( 6+2*r, 3+7*c, s->board[r][c] );
            }
        }
    }
}

int main(void) {
    struct termios tty;
    int termconf, direction;
    char input;
    struct GameState game;
    tcgetattr(STDIN_FILENO, &tty);

    termconf = tty.c_lflag;
    tty.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &tty);

    printf("\033[2J");
    srand( time( NULL ) );
    InitGame( &game );
    PrintBoard();
    while (1) {
        PrintState( &game );
        PrintAt( 18, 1, "" );
        input = getchar();
        switch (input) {
            case 'w':
            case 'W':
            case 'k':
            case 'K':
            case '8':
                direction = 2;
                break;
            case 'a':
            case 'A':
            case 'h':
            case 'H':
            case '4':
                direction = 0;
                break;
            case 's':
            case 'S':
            case 'j':
            case 'J':
            case '2':
                direction = 3;
                break;
            case 'd':
            case 'D':
            case 'l':
            case 'L':
            case '6':
                direction = 1;
                break;
            case 'x':
            case 'X':
            case 'q':
            case 'Q':
            case '.':
                direction = 10;
                break;
            case 27:
                input = getchar();
                if (input == '[') {
                    input = getchar();
                    switch (input) {
                        case 'A':
                            direction = 2;
                            break;
                        case 'B':
                            direction = 3;
                            break;
                        case 'C':
                            direction = 1;
                            break;
                        case 'D':
                            direction = 0;
                            break;
                    }
                } else {
                    direction = 10;
                }
                break;
        }
        if (direction < 4) {
            ShiftNumbers(&game, direction);
        } else if (direction == 10) {
            break;
        }
    }

    tty.c_lflag = termconf;
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &tty);

    return 0;
}

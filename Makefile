BUILDDIR = _build
SOLO = c2048-tty-solo c2048-screen-solo
USINGLIB = c2048-tty-using-lib c2048-screen-using-lib
EXECUTABLES = $(SOLO) $(USINGLIB)
LIBFILES = lib2048.so lib2048.o
SOLODIR = $(foreach f,$(SOLO),$(BUILDDIR)/$(f))
USINGLIBDIR = $(foreach f,$(USINGLIB),$(BUILDDIR)/$(f))

RMDIR = $(RM) -r
MKDIR = mkdir -p

.PHONY: clean default all solo using-lib tty screen $(EXECUTABLES) lib2048.so

default: c2048-tty-solo

all: $(SOLODIR) $(USINGLIBDIR)

$(BUILDDIR)/%-using-lib: %.c $(BUILDDIR)/lib2048.so
	$(MKDIR) $(BUILDDIR) ; \
	$(CC) -Wall $< -L. -L$(BUILDDIR) -l2048 -o $@

$(BUILDDIR)/%-solo: %.c lib2048.c
	$(MKDIR) $(BUILDDIR) ; \
	$(CC) -Wall $< lib2048.c -o $@

$(BUILDDIR)/lib2048.o: lib2048.c
	$(MKDIR) $(BUILDDIR) ; \
	$(CC) -c -Wall -Werror -fpic $< -o $@

$(BUILDDIR)/lib2048.so: $(BUILDDIR)/lib2048.o
	$(CC) -shared $< -o $@

$(EXECUTABLES): c2048-%: $(BUILDDIR)/c2048-%
lib2048.so: $(BUILDDIR)/lib2048.so
solo: $(SOLO)
using-lib: $(USINGLIB)
tty: c2048-tty-solo c2048-tty-using-lib
screen: c2048-screen-solo c2048-screen-using-lib

clean:
	$(RMDIR) $(BUILDDIR)

#include <stdlib.h>
#include "lib2048.h"

/*
Implementation of 2048 in C. Assume bugs aplenty. Bring bugspray.
I don't know C. At all. This is all written by DuckDuckGo.

Future plans:
* Make the library support different board sizes. Because why not.
*/

void PlaceNewNumber(struct GameState *s) {
    int r,c,k,i;
    int n = 0;
    int free[16][2];
    for (r=0;r<4;r++) {
        for (c=0;c<4;c++) {
            if ( s->board[r][c] == 0 ) {
               free[n][0] = r;
               free[n][1] = c;
               n++;
            }
        }
    }
    k = rand()%n;
    if ( rand()%10 < 9 ) {
        i = 2;
    } else {
        i = 4;
    }
    s->board[ free[k][0] ][ free[k][1] ] = i;
}

void ReverseBoard(struct GameState *s) {
    int r,c,t;
    for (r=0;r<4;r++) {
        for (c=0;c<2;c++) {
            t = s->board[r][c];
            s->board[r][c] = s->board[r][3-c];
            s->board[r][3-c] = t;
        }
    }
}

void InvertBoardXY(struct GameState *s) {
    int r,c,t;
    for (r=0;r<4;r++) {
        for (c=r;c<4;c++) {
            if ( r == c ) {
                continue;
            }
            t = s->board[r][c];
            s->board[r][c] = s->board[c][r];
            s->board[c][r] = t;
        }
    }
}

void MergeNumbers(struct GameState *s) {
    int r,c,n;
    for (r=0;r<4;r++) {
        for (c=0;c<3;c++) {
            if ( s->board[r][c] == s->board[r][c+1] ) {
                s->board[r][c] +=s->board[r][c+1];
                s->score += s->board[r][c];
                for (n=c+1;n<4;n++) {
                    if ( n == 3 ) {
                        s->board[r][n] = 0;
                    } else {
                        s->board[r][n] = s->board[r][n+1];
                    }
                }
            }
        }
    }
}

void SetGameStatus(struct GameState *s) {
    int r,c;
    //Find any zeroes, possible merges, or 2048
    for (r=0;r<4;r++) {
        for (c=0;c<4;c++) {
            if ( s->board[r][c] >= 2048 ) {
                s->status = 1;
                return;
            }
            if ( s->board[r][c] == 0 ) {
                s->status = 0;
                return;
            }
            if ( r<3 && ( s->board[r][c] == s->board[r+1][c] ) ) {
                s->status = 0;
                return;
            }
            if ( c<3 && ( s->board[r][c] == s->board[r][c+1] ) ) {
                s->status = 0;
                return;
            }
        }
    }
    //No empty spaces, no possible moves, not reached 2048. Lost.
    s->status = 2;
}

void ShiftNumbers(struct GameState *s, int dir) {
    int r,c,n,tb[4][4];
    //dir = 0 -> Left
    //dir = 1 -> Right (Reverse, Left, Reverse)
    //dir = 2 -> Up (Invert, Left, Invert)
    //dir = 3 -> Down (Invert, Reverse, Left, Reverse, Invert)
    for (r=0;r<4;r++) {
        for (c=0;c<4;c++) {
            tb[r][c] = s->board[r][c];
        }
    }
    if (dir & 2) { InvertBoardXY(s); }
    if (dir & 1) { ReverseBoard(s); }
    for (r=0;r<4;r++) {
        for (c=0;c<3;c++) {
            if ( s->board[r][c] != 0 ) {
                continue;
            }
            for (n=c+1;n<4;n++) {
                if ( s->board[r][n] != 0 ) {
                    s->board[r][c] = s->board[r][n];
                    s->board[r][n] = 0;
                    break;
                }
            }
        }
    }
    MergeNumbers(s);
    if (dir & 1) { ReverseBoard(s); }
    if (dir & 2) { InvertBoardXY(s); }
    s->rounds++;
    for (r=0;r<4;r++) {
        for (c=0;c<4;c++) {
            if (tb[r][c] != s->board[r][c]) {
                PlaceNewNumber(s);
                SetGameStatus(s);
                return;
            }
        }
    }
}

void InitGame(struct GameState *s) {
    int r,c;
    s->status = 0;
    s->score = 0;
    s->rounds = 0;
    for (r=0; r<4; r++) {
        for (c=0; c<4; c++) {
            s->board[r][c] = 0;
        }
    }
    PlaceNewNumber(s);
    PlaceNewNumber(s);
}

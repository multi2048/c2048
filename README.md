# c2048
A C implementation of the game 2048.

## lib2048.h, lib2048.c
Base game logic. Used by the different interfaces.
Can be compiled to a shared library or included to a single binary.

## c2048-tty.c
Sinple TTY-based interface. Doesn't make any assumptions about the console's
capabilities, should work on an actual teletype terminal.

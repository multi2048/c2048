/*
Implementation of 2048 in C. Assume bugs aplenty. Bring bugspray.
I don't know C. At all.

Future plans:
* Make a library that can be used by different frontends.
*/

struct GameState {
    int status;
    int score;
    int rounds;
    //int boardsize[2];
    int board[4][4];
};

void ShiftNumbers(struct GameState *s, int dir);

void InitGame(struct GameState *s);

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lib2048.h"

/*
Implementation of 2048 in C. Assume bugs aplenty. Bring bugspray.
I don't know C. At all.

Future plans:
* Make a library that can be used by different frontends.
*/

void PrintGame(struct GameState *s) {
    int r,c;
    printf("========================\n");
    printf(" Rounds: %d\n", s->rounds);
    printf(" Score:  %d\n", s->score);
    printf("\n");
    for (r=0;r<4;r++) {
        printf("+------+------+------+------+\n");
        for (c=0;c<4;c++) {
            printf("| %4d ", s->board[r][c]);
        }
        printf("|\n");
    }
    printf("+------+------+------+------+\n");
}

int main(void) {
    char c;
    srand(time(NULL));
    struct GameState game;
    InitGame(&game);
    printf("Welcome to the game of 2048!\n");
    while (1) {
        PrintGame(&game);
        printf("(L)eft, (U)p, (R)ight, (D)own, (Q)uit\n");
        printf("Command: ");
        scanf(" %s", &c);
        printf("\n");
        if (c == 'l') {
            ShiftNumbers(&game, 0);
        } else if (c == 'r') {
            ShiftNumbers(&game, 1);
        } else if (c == 'u') {
            ShiftNumbers(&game, 2);
        } else if (c == 'd') {
            ShiftNumbers(&game, 3);
        } else if (c == 'q') {
            printf("Bye-bye.\n");
            break;
        }
    }
    return 0;
}
